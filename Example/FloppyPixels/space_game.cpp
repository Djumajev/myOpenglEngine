bool player_dead = true;
float level = 1;

//Player obj
struct pixel {
	int x = -300;
	float vector = 0;
	int score = 0;
} player;

//Enemy
struct enemy {
	int x;
	int y;

	enemy() {
		x = 1366 / 2 + 10;
		y = rand() % 768 / 2 - rand() % 768 / 2;
	}

	void set_x(int newx) {
		x = newx;
	}
};

vector<enemy*>enemys;

//Create star sky
struct st {
	int x = 1366 / 2;
	int y = 0;
	st(int x, int y) {
		this->x = x;
		this->y = y;
	}
};

vector<st*> stars;

void star_sky()
{
	for (int i = 0; i < 500; i++) {
		stars.push_back(new st(rand() % 1366 / 2 - rand() % 1366 / 2, rand() % 768 / 2 - rand() % 768 / 2));
	}

}

void loos() {

	a.set_color(0, 0, 1);
	a.write(-100, 30, (string)"Tap to continue");
	if (a.is_clicked()) {
		int* click = new int[2];
		click = a.get_click(true);
		if (click[0] >= -100 && click[0] <= 100 && click[1] >= 30 && click[1] <= 50)
			start_space = false;
	}
}

void space_game() {
	//Draw stars
	a.set_color(1, 1, 1);
	//a.log_info(to_string(stars.size()));
	for (auto x : stars) {
		a.quad(x->x, x->y, 2);
	}

	//player
	{
		a.set_color(0.5, 0, 0);
		a.quad(player.x + 7 + 15, player.vector, 20, 4, true);
		a.set_color(0.5, 0.5, 0.4);
		a.quad(player.x, player.vector + 15, 14, 18, true);
		a.set_color(0.3, 0.3, 0.3);
		a.quad(player.x, player.vector, 30, 24, true);
		a.quad(player.x + 15, player.vector, 14);
		a.set_color(0.2, 0.2, 0.2);
		a.quad(player.x - 15, player.vector, 14);
		a.set_color(0.8, 0.8, 0);
		for (int i = 0; i < 21; i += 3) {
			a.quad(player.x - 8 + i, player.vector + 4, 2);
			a.quad(player.x - 8 + i, player.vector, 2);
			a.quad(player.x - 8 + i, player.vector - 4, 2);
		}
		a.set_color(0, 1, 0);
		a.quad(player.x + 15, player.vector, 2);
		a.set_color(0, 0, 0.2);
		a.quad(player.x + 2, player.vector + 18, 10, 4, true);

		a.set_color(0, 0.5, 1);
		a.write(1366 / 2 - 200, 768 / 2 - 30, (string)"Score: ");
		a.write(1366 / 2 - 100, 768 / 2 - 30, to_string(player.score));
	}

	//Enemy
	{
		if (!a.is_timer()) a.start_timer();
		double time = a.get_time();
		if (time > 0.5) {
			for (int i = 0; i < level; i++)
				enemys.push_back(new enemy());
			a.stop_timer();
		}

		for (int i = 0; i < enemys.size(); i++) {
			a.set_color(0.8, 0.8, 0.8);
			a.quad(enemys[i]->x, enemys[i]->y, 10);
			a.set_color(0.3, 0.3, 0.3);
			a.quad(enemys[i]->x + 2, enemys[i]->y + 1, 5);
			a.quad(enemys[i]->x - 3, enemys[i]->y - 3, 8);

			a.set_color(0.8, 0.6, 0);
			for (int x = 0; x < 10; x++)
				a.quad(enemys[i]->x + 8 + rand() % 20, enemys[i]->y + rand() % 12 - 6, 2);

			enemys[i]->set_x(enemys[i]->x - 2);

			if (enemys[i]->x <= -1366 / 2) {
				enemys.erase(enemys.begin() + i);
				if (!player_dead) player.score++;
				if (level < 10)level += 0.1;
			}

			if (player.vector + 24 >= enemys[i]->y + 7 && player.vector - 24 <= enemys[i]->y - 7)
				if (player.x + 30 >= enemys[i]->x - 7 && player.x - 30 <= enemys[i]->x + 7) {
					player_dead = true;
				}
		}
	}

	//Flying
	if (!player_dead)
		if (a.is_clicked()) {
			player.vector += 40;
			a.get_click(true);
		}

	player.vector -= 0.5;

	//Loosing
	if (player.vector < -768 / 2 || player.vector > 768 / 2) {
		player_dead = true;
	}

	if (player_dead)
		loos();
}