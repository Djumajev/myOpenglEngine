#pragma once

void game_1() {
	player.score = 0;
	player.vector = 0;
	enemys.clear();
	level = 1;
	player_dead = false;
	start_space = true;
}

void game_2() {
	start_music = true;
	a.stop_timer();
}

void exit_func() {
	if (con.isConnected()) con.Disconnect();
	exit(0);
}

void neiron() {
	querry.setConnection(&con);
	//Enter genom from db
	for (int i = 0; i < snakes_count; i++) {
		snakes.push_back(new snake(-300 + 6, 0));
		snakes[i]->rgb[0] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		snakes[i]->rgb[1] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		snakes[i]->rgb[2] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

		querry.setCommandText("SELECT id FROM neuron_lvl1_1 ORDER BY id DESC LIMIT 1 ;");
		querry.Execute();

		int max_id;
		while (querry.FetchNext()) {
			max_id = querry[1].asDouble();
		}

		//Neirons first lvl
		for (int n = 0; n < 4; n++) {
			string str = "SELECT W1, W2, W3, W4 FROM neuron_lvl1_"
				+ to_string(n + 1)
				+ " WHERE id ="
				+ to_string(max_id)
				+ ";";
			querry.setCommandText(str.c_str());
			querry.Execute();
			while (querry.FetchNext()) {
				snakes[i]->neirons_1[n]->set_w(querry[1].asDouble(), querry[2].asDouble(), querry[3].asDouble(), querry[4].asDouble());
			}
		}

		//Neirons second lvl
		for (int n = 0; n < 2; n++) {
			string str = "SELECT W1, W2, W3, W4 FROM neuron_lvl2_"
				+ to_string(n + 1)
				+ " WHERE id ="
				+ to_string(max_id)
				+ ";";
			querry.setCommandText(str.c_str());
			querry.Execute();
			while (querry.FetchNext()) {
				snakes[i]->neirons_2[n]->set_w(querry[1].asDouble(), querry[2].asDouble(), querry[3].asDouble(), querry[4].asDouble());
			}
		}
	}

	//Enter map from db
	querry.setCommandText("SELECT * FROM obstacles WHERE map_id = 1;");
	querry.Execute();
	while (querry.FetchNext()) {
		obstacles.push_back(new obstacle(querry[2].asDouble(), querry[3].asDouble(), querry[4].asDouble(), querry[5].asDouble()));
	}
	start_snake = true;
}

void chat() {
	querry.setConnection(&con);
	start_chat = true;
}