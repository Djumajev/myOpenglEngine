void music()
{
	if (!a.is_timer()) a.start_timer();
	if (a.get_time() <= 30) {
		a.set_color(0.8, 0, 0);
		a.write(1366 / 2 - 100, -768 / 2 + 30, to_string(a.get_time()));
		ShowCursor(false);

		//Get keyboard key
		unsigned char key = a.get_keyboard();

		//To clear screen
		static bool clear = false;

		static vector<int*>Points;
		static vector<int*>Boom;

		//Get mouse
		int* mouse = new int[2];
		mouse = a.get_mouse();
		Points.push_back(mouse);

		//Keys effects
		if (key == 'w') {
			a.clear_screen();
			clear = true;
		}
		else if (key == 'e') {
			Boom.push_back(mouse);
		}
		else if (a.is_clicked())
			Boom.push_back(a.get_click(true));

		//Draw mouse 
		for (auto x : Points) {
			//Point colors
			if (x[0] % 2) a.set_color(1, 0, 1);
			else if (x[0] % 3) a.set_color(1, 1, 0);
			else a.set_color(0, 1, 0);

			a.quad(x[0], x[1], 2);
			a.quad(-x[0], x[1], 2);
			a.quad(x[0], -x[1], 2);
			a.quad(-x[0], -x[1], 2);
		}
		//Draw boom
		for (auto x : Boom) {
			for (int i = 0; i < 360; i += 12) {
				for (int r = 0; r < 100; r += 10) {
					a.quad(x[0] - r * cos(i*3.14 / 180), x[1] - r * sin(i*3.14 / 180), 2);
					a.quad(-x[0] - r * cos(i*3.14 / 180), x[1] - r * sin(i*3.14 / 180), 2);
					a.quad(x[0] - r * cos(i*3.14 / 180), -x[1] - r * sin(i*3.14 / 180), 2);
					a.quad(-x[0] - r * cos(i*3.14 / 180), -x[1] - r * sin(i*3.14 / 180), 2);
				}
			}
		}

		//Clear screen
		if (clear) {
			Points.clear();
			Boom.clear();
			clear = false;
		}

	}
	else start_music = false;
}
