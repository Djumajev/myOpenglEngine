#pragma once

#include <iostream>
#include <cmath>
#include "windows.h"
#include <GL/glut.h> // OpenGL
#include <string>
#include <ctime>
#include <thread>
#include <vector>
#include <fstream>
#include <algorithm>
#include <MMsystem.h>// Sounds
#include <functional> //Lambda
#include <fstream>

using namespace std;

class engine
{
public:

	//constructor
	engine(bool show_console);
	//constructor
	engine();
	//Initilization
	void init(int argc, char** argv, int width, int height);
	//Clear screen
	void clear_screen();
	//Get mouse coordinates
	int* get_mouse();
	//Is clicked
	bool is_clicked();
	//Get mouse click
	int* get_click(bool change_state);
	//Set fullscreen
	void set_fullscreen(bool is_fullscreen);
	//Is fullscreen
	bool is_fullscreen();
	//Get Keyboard
	unsigned char get_keyboard();
	//Get Special keys Keyboard
	string get_special_keyboard();
	//Text area
	string text_area(int x, int y, int size_x, int size_y, string input, float font_red = 0, float font_green = 0, float font_blue = 0);
	//Hexes
	void hexes(float x, float y, int size, bool is_filled);
	//Set delay
	void set_screen_delay(int delay);
	//Get delay
	int get_screen_delay();
	//Get screen resolution
	int* get_resolution();
	//Button function
	void button_func(int ID);
	//Circles
	void circle(float x, float y, float R, int amountSegments, bool is_filled = true);
	//Quadrilateral
	void quad(float x, float y, int size_x, int size_y, bool is_filled = true);
	//Quad
	void quad(float x, float y, int size_x, bool is_filled = true);
	//Text with string
	void write(float x, float y, string text);
	//Line
	void line(int start_x, int start_y, int end_x, int end_y);
	//Set color
	void set_color(float red, float green, float blue);
	//Set line width
	void set_linewidth(GLfloat linewidth);
	//Set background
	void set_background(float red, float green, float blue);
	//Strat timer
	void start_timer();
	//Stop timer
	void stop_timer();
	//Return miliseconds
	int get_time();
	//Is timer started
	bool is_timer();
	//Play sound
	void play_sound(LPCTSTR file_name);
	//Is sound playing
	bool is_sound();
	//Log errors
	void log_error(string msg);
	//Log info
	void log_info(string msg);
	//Log message    white
	void log_msg(string msg);
	//Log message    colorfull
	void log_msg(string msg, int red, int green, int blue);

	//destructor
	virtual ~engine();

private:
	//Timer
	int timer_int = 0;
	bool started = false;
	//Sounds
	bool is_sound_p = false;

	//Intialization
	void Intialization();
	//Main OpenGL function
	void OpenGLMain(int, char**);
	//Timer
	static void timer(int);
	//Draw
	static void draw();
	//Mouse click
	static void click(int, int, int, int);
	//Keyboard normal keys
	static void process_normal_keys(unsigned char, int, int);
	//Keyboard special keys
	static void key_board(int, int, int);
	//Mouse
	static void mouse_controller(int, int);
	//Timer
	void time_counter();
};

