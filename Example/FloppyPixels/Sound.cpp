#pragma once
//Is sound playing
bool engine::is_sound()
{
	return is_sound_p;
}

//Play sound
/*        !Important!   obj_name.play_sound(L"file location\\file_name.wav");   */
void engine::play_sound(LPCTSTR file_name)
{
	is_sound_p = true;
	thread* newth;
	//Sound on new thread
	function<void(LPCTSTR)> sound = [this](LPCTSTR file)
	{
		PlaySound(file, NULL, SND_FILENAME | SND_SYNC);
		is_sound_p = false;
	};
	newth = new thread(sound, file_name);
	newth->detach();
}
