#pragma once
#include <SQLAPI.h>

SAConnection con;
SACommand querry;

int snake_time = 2;
const int screen_x = 600, screen_y = 400;
const int point_x = 290, point_y = 0;
unsigned int generation = 0;
const int snakes_count = 500;

struct obstacle {
	int obstacles_x, obstacles_y;
	int x, y;
	obstacle(int x2, int y2, int size_x, int size_y) {
		x = x2;
		y = y2;
		obstacles_x = size_x;
		obstacles_y = size_y;
	}
};

struct n_lvl_1 {
	double sum;
	float w[4];

	void core(float obs_x, float obs_y, float point_x, float point_y) {
		sum = sin(obs_x * w[0] + obs_y * w[1] + point_x * w[2] + point_y * w[3]);
	}

	void set_w(float w1, float w2, float w3, float w4) {
		w[0] = w1;
		w[1] = w2;
		w[2] = w3;
		w[3] = w4;
	}

	void set_rand_w() {
		for(int ww = 0; ww < 4; ww++)
			w[ww] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	}

};


struct n_lvl_2 {
	double sum;
	float w[4];

	void core(float n1, float n2, float n3, float n4) {
		sum = sin(n1 * w[0] + n2 * w[1] + n3 * w[2] + n4 * w[3]);
	}

	void set_w(float w1, float w2, float w3, float w4) {
		w[0] = w1;
		w[1] = w2;
		w[2] = w3;
		w[3] = w4;
	}

	void set_rand_w() {
		for (int ww = 0; ww < 4; ww++)
			w[ww] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	}
};

class snake {
public:
	int x, y;
	float rgb[3];
	int ticks = 0;
	int movement_x, movement_y;

	vector<n_lvl_1*> neirons_1;
	vector<n_lvl_2*> neirons_2;

	snake(int x2, int y2) {
		x = x2;
		y = y2;
		for (int i = 0; i < 4; i++) {
			neirons_1.push_back(new n_lvl_1());
			//neirons_1[neirons_1.size() - 1]->set_rand_w();
		}

		for (int i = 0; i < 4; i++) {
			neirons_2.push_back(new n_lvl_2());
			//neirons_2[neirons_2.size() - 1]->set_rand_w();
		}
	}

	void think(int obs_x, int obs_y, int point_x, int point_y) {
		for (auto x : neirons_1) {
			x->core(obs_x, obs_y, point_x, point_y);
		}
		for (auto x : neirons_2) {
			x->core(neirons_1[0]->sum, neirons_1[1]->sum, neirons_1[2]->sum, neirons_1[3]->sum);
		}
		for (int i = 0; i < neirons_2.size(); i++) {
			if (i % 2 == 0) {
				if (neirons_2[i]->sum > 0) movement_x = 1;
				else if (neirons_2[i]->sum < 0) movement_x = -1;
				else movement_x = 0;
			}
			else {
				if (neirons_2[i]->sum > 0) movement_y = 1;
				else if (neirons_2[i]->sum < 0) movement_y = -1;
				else movement_y = 0;
			}
		}
	}


	~snake() {
	}
};