#pragma once
void chat_main() {
	//Back button
	a.set_color(0.8, 0.2, 0.2);
	a.write(-600, 300, string("Back"));

	int* click = new int[2];
	if (a.is_clicked()) click = a.get_click(false);
	if (click[0] >= -600 && click[0] <= -500 && click[1] >= 300 && click[1] <= 350) {
		start_chat = false;
	}

	a.set_color(0.2, 0.2, 0.2);
	static string man_msg = "Type here your message";
	static string nick = "No name";
	man_msg = a.text_area(0, -200, 600, 50, man_msg, 0, 0.8, 0);
	if (man_msg.size() > 50) man_msg.resize(50);
	a.set_color(0, 0, 0);
	nick = a.text_area(300, 300, 200, 50, nick, 1, 0, 0);
	if (nick.size() > 8) nick.resize(8);

	a.set_color(0.6, 0.2, 0.2);
	a.quad(360, -200, 100, 50, false);
	a.write(320, -200, (string)"Send");

	//Send a msg
	if ((click[0] >= 310 && click[0] <= 410) && (click[1] >= -250 && click[1] <= -150) && man_msg.size() > 0) {
		string quer = "INSERT INTO chat_messages(message, time, nickname) VALUES('" + man_msg + "', CURRENT_TIME(), '" + nick + "');";
		querry.setCommandText(quer.c_str());
		querry.Execute();
		man_msg = "";
		a.get_click(true);
	}

	//Show last msg
	string quer = "SELECT message, time, nickname FROM chat_messages ORDER BY id DESC LIMIT 10;";
	querry.setCommandText(quer.c_str());
	querry.Execute();

	a.set_color(0, 0.6, 0);
	int y = -150;
	while (querry.FetchNext()) {
		a.set_color(0, 0.8, 0);
		a.write(-200, y, string(querry[1].asString()));
		a.set_color(0.2, 0.2, 0.2);
		a.write(-300, y, string(querry[2].asString()));
		a.write(-400, y, string(querry[3].asString()));
		y += 50;
	}

}