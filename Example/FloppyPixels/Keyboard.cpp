#pragma once
//Get Normal keys Keyboard
unsigned char engine::get_keyboard()
{
	unsigned char a = PressedKey;
	PressedKey = NULL;
	return a;
}

//Get Special keys Keyboard
string engine::get_special_keyboard()
{
	string a = PressedSpecialKey;
	PressedSpecialKey = "";
	return a;
}

//Keyboard Normal keys
void engine::process_normal_keys(unsigned char key, int x, int y)
{
	PressedKey = key;
}

//Keyboard Special keys
void engine::key_board(int key, int x, int y)
{
	PressedSpecialKey = key;
}
