#include "stdafx.h"
#include "engine.h"

/*********Variables********/

engine a(true);
bool start_space = false, start_music = false, start_snake = false, start_chat = false;

/*********Games***********/
#include "Space_game.cpp"
#include "Music_game.cpp"
#include "Snake.cpp"
#include "Chat.cpp"

/*******Menu functions******/
#include "Menu_func.cpp"

void engine::draw() {
	
	//Show cursor
	ShowCursor(true);

	//Set background
	a.set_background(0, 0, 0);

	//Set fullscreen
	if(!a.is_fullscreen()) a.set_fullscreen(false);

	if (con.isAlive()) {
		a.set_color(0, 1, 0);
		a.write(-50, 300, string("Online"));
		a.line(-50, 295, 22, 295);
	}
	else {
		start_chat = false;
		start_snake = false;
		if (!a.is_timer()) a.start_timer();
		a.set_color(1, 0, 0);
		a.write(-50, 300, string("Offline"));
		a.line(-50, 295, 22, 295);
		if (a.get_time() > 5 && !start_music && !start_space) {
			a.stop_timer();
			try {
				con.Connect(
					"localhost@skynet",
					"chat",
					"chat1488",
					SA_MySQL_Client
				);
				if (con.isConnected())
					a.log_info("DB connected");
			}
			catch (SAException &) {
				a.log_error("DB not connected");
			}
		}
	}
	
	//Play music
	if(!a.is_sound())a.play_sound(L"menu.wav");

	//For blinking
	static float green = 1;
	
	//Games & menu
	if (start_space)
		space_game();
	else if (start_music)
		music();
	else if (start_snake && con.isAlive())
		for (int i = 0; i <= snake_time; i++) snake_game();
	else if (start_chat && con.isAlive())
		chat_main();
	else {

		//Empty everything
		snakes.clear();
		obstacles.clear();
		enemys.clear();

		a.set_color(0, green, 0);

		a.write(100, 30, (string)"Last score:");
		a.write(210, 30, to_string(player.score));
		a.write(100, 0, (string)"Click to start #2 game");
		a.write(-300, 0, (string)"Click to start #1 game");
		if(con.isAlive()) a.write(-300, 200, (string)"Genom");
		if(con.isAlive()) a.write(100, 200, (string)"Chat");
		a.write(-50, -768/2+50, (string)"EXIT");
		green -= 0.003;
		if (green <= 0.1) green = 1;

		//Clickble elements of menu
		if (a.is_clicked()) {
			int* click = new int[2];
			click = a.get_click(true);
			if (click[0] >= 100 && click[0] <= 350 && click[1] >= -10 && click[1] <= 50) {
				game_1();
			}
			else if (click[0] >= -300 && click[0] <= -50 && click[1] >= -10 && click[1] <= 50) {
				game_2();
			}
			else if (click[0] >= -50 && click[0] <= 10 && click[1] >= -768 / 2 + 30 && click[1] <= -768 / 2 + 70) {
				exit_func();
			}
			else if (click[0] >= -300 && click[0] <= -50 && click[1] >= 190 && click[1] <= 250 && con.isAlive()) {
				neiron();
			}
			else if (click[0] >= 100 && click[0] <= 350 && click[1] >= 190 && click[1] <= 250 && con.isAlive()) {
				chat();
			}
		}
	}
}

int main(int argc, char** argv) {
	star_sky();
	srand(time(0));
	a.set_screen_delay(1);
	a.init(argc, argv, 1366, 768);

	return 0;
}

