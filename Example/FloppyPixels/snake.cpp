#pragma once
#include "Snake.h"

vector<obstacle*> obstacles;
vector<snake*> snakes;

void snake_logic();

void snake_game() {
	a.set_background(0.2, 0.2, 0.2);

	//Back button
	a.set_color(0.8, 0.2, 0.2);
	a.write(-600, 300, string("Back"));

	int* click = new int[2];
	if (a.is_clicked()) click = a.get_click(false);
	if (click[0] >= -600 && click[0] <= -500 && click[1] >= 300 && click[1] <= 350) {
		start_snake = false;
	}

	//Obstacles
	a.set_color(1, 0.5, 0.5);
	for (auto x : obstacles)
		a.quad(x->x, x->y, x->obstacles_x, x->obstacles_y, false);
	//Point
	a.set_color(0, 1, 0);
	a.quad(point_x, point_y, 10);

	//Time
	a.set_color(1, 0, 0);
	a.write(400, 0, to_string((int)a.get_time()));

	//Generation
	a.write(400, 50, to_string(generation));

	//Draw and snakes
	for (auto x : snakes) {
		a.set_color(x->rgb[0], x->rgb[1], x->rgb[2]);
		a.quad(x->x, x->y, 10);
	}

	for (int i = 0; i < snake_time; i++) {
		snake_logic();
	}
}

int min_err()
{
	static int min = 0;
	for (int i = 0; i < snakes.size(); i++) {
		if (sqrt(pow(point_x - snakes[min]->x, 2) + pow(point_y - snakes[min]->y, 2)) + snakes[min]->ticks > sqrt(pow(point_x - snakes[i]->x, 2) + pow(point_y - snakes[i]->y, 2)) + snakes[i]->ticks)
		{
			min = i;
			return min_err();
		}
		else continue;
	}
	return min;
}

void snake_logic() {
	if (!a.is_timer()) a.start_timer();

	bool end = false;

	//Movement
	for (auto x : snakes) {
		//Search for nearest obstacle in X and Y
		int min_X = 0, min_Y = 0;
		for (auto obs : obstacles) {
			int obs_x = obs->x - obs->obstacles_x / 2 - x->x - 5;
			int obs_y = obs->obstacles_y / 2 - x->y - 5;
			if (pow(obs_x, 2) < pow(min_X, 2)) min_X = obs_x;
			if (pow(obs_y, 2) < pow(min_Y, 2)) min_Y = obs_y;
		}
		x->think(min_X, min_Y, point_x - x->x, point_y - x->y);
		//Check obs
		bool obs_check = true;

		for (auto obs : obstacles) {
			if (x->x + x->movement_x > obs->x - obs->obstacles_x / 2 - 5 && x->x + x->movement_x < obs->x + obs->obstacles_x / 2 + 5) {
				if (x->y + x->movement_y > obs->y - obs->obstacles_y / 2 - 5 && x->y + x->movement_y < obs->y + obs->obstacles_y / 2 + 5) {
					obs_check = false;
					break;
				}
			}
		}
		if (obs_check) {
			x->x += x->movement_x;
			x->y += x->movement_y;
		}

		if (x->x >= point_x - 5 && x->x <= point_x + 5 && x->y >= point_y - 5 && x->y <= point_y + 5) {
			end = true;
		}

		if (x->movement_x != 0 && x->movement_y != 0)
			x->ticks++;
		x->movement_x = 0;
		x->movement_y = 0;

	}

	if (snakes[snakes.size() - 1]->ticks > 1000 || end) {
		//if (a.get_time() > 3 || end) {
		a.stop_timer();
		generation++;


		int min = min_err();
		for (int n = 0; n < 4; n++) {
			string str = "INSERT INTO neuron_lvl1_"
				+ to_string(n + 1)
				+ " (W1, W2, W3, W4) VALUES ("
				+ to_string(snakes[min]->neirons_1[n]->w[0]) + ", "
				+ to_string(snakes[min]->neirons_1[n]->w[1]) + ", "
				+ to_string(snakes[min]->neirons_1[n]->w[2]) + ", "
				+ to_string(snakes[min]->neirons_1[n]->w[3])
				+ ");";
			querry.setCommandText(str.c_str());
			querry.Execute();
		}
		for (int n = 0; n < 2; n++) {
			string str = "INSERT INTO neuron_lvl2_"
				+ to_string(n + 1)
				+ " (W1, W2, W3, W4) VALUES ("
				+ to_string(snakes[min]->neirons_2[n]->w[0]) + ", "
				+ to_string(snakes[min]->neirons_2[n]->w[1]) + ", "
				+ to_string(snakes[min]->neirons_2[n]->w[2]) + ", "
				+ to_string(snakes[min]->neirons_2[n]->w[3])
				+ ");";
			querry.setCommandText(str.c_str());
			querry.Execute();
		}
		querry.setCommandText("SELECT id FROM neuron_lvl1_1 ORDER BY id DESC LIMIT 1 ;");
		querry.Execute();

		int max_id;
		while (querry.FetchNext()) {
			max_id = querry[1].asDouble();
		}

		string str = "INSERT INTO genom(lvl1_1_id, lvl1_2_id, lvl1_3_id, lvl1_4_id, lvl2_1_id, lvl2_2_id) VALUES("
			+ to_string(max_id) + ", "
			+ to_string(max_id) + ", "
			+ to_string(max_id) + ", "
			+ to_string(max_id) + ", "
			+ to_string(max_id) + ", "
			+ to_string(max_id)
			+ ");";
		querry.setCommandText(str.c_str());
		querry.Execute();

		static int map = 1;
		if (end) {
			generation = 0;
			obstacles.clear();
			if (map < 8) map++;
			else map = 1;
			string str = "SELECT * FROM obstacles WHERE map_id = "
				+ to_string(map) + ";";
			querry.setCommandText(str.c_str());
			querry.Execute();
			while (querry.FetchNext()) {
				obstacles.push_back(new obstacle(querry[2].asDouble(), querry[3].asDouble(), querry[4].asDouble(), querry[5].asDouble()));
			}
		}
		else if (generation > 30) {
			generation = 0;
			map = 1;
			obstacles.clear();
			querry.setCommandText("SELECT * FROM obstacles WHERE map_id = 1;");
			querry.Execute();
			while (querry.FetchNext()) {
				obstacles.push_back(new obstacle(querry[2].asDouble(), querry[3].asDouble(), querry[4].asDouble(), querry[5].asDouble()));
			}
		}

		for (int i = 0; i < snakes.size(); i++) {
			if (i != min) {
				//Copy from best
				for (int ii = 0; ii < snakes[i]->neirons_1.size(); ii++)
					for (int w = 0; w < 4; w++)
						snakes[i]->neirons_1[ii]->w[w] = snakes[min]->neirons_1[ii]->w[w];

				for (int ii = 0; ii < snakes[i]->neirons_2.size(); ii++)
					for (int w = 0; w < 4; w++)
						snakes[i]->neirons_2[ii]->w[w] = snakes[min]->neirons_2[ii]->w[w];

				//Mutation
				int ran = rand() % 2 + 1;
				if (ran % 2 == 0) snakes[i]->neirons_1[rand() % 4]->w[rand() % 4] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
				else {
					snakes[i]->neirons_1[rand() % 4]->w[rand() % 4] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
					snakes[i]->neirons_2[rand() % 2]->w[rand() % 4] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
				}
			}
			snakes[i]->x = -300 + 6;
			snakes[i]->y = 0;
			snakes[i]->ticks = 0;
		}

	}

}
