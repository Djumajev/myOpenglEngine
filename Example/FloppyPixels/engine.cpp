#pragma once
#include "stdafx.h"
#include "engine.h"
//#pragma comment(lib,"Winmm.dll")

/**********Variables**********/

//Scree size
int We = 0;
int He = 0;
bool fullscreen;
float backgroung_RGB[] = { 1, 1, 1 };
int screen_delay = 1;

//Mouse
int MouseClickX, MouseClickY,
MouseX, MouseY;
bool IsClicked = false;

//Keyboard
int PressedKey;
string PressedSpecialKey = "";

//Consts
const float PI = 3.14;

/*********My functions**************/
#include "Drawble.cpp"
#include "Keyboard.cpp"
#include "Mouse.cpp"
#include "Opengl.cpp"
#include "Screen.cpp"
#include "Sound.cpp"
#include "Timer.cpp"
#include "Log.cpp"

//Constructor
engine::engine(bool show_console)
{
	//Hide console
	if(!show_console)
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	else {
		#ifdef _WIN32
			// Init ANSI escapes for windows to use color messages
			HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
			if (hOut == INVALID_HANDLE_VALUE) {
				this->log_error(to_string(GetLastError()));
			}
			DWORD dwMode = 0;
			if (!GetConsoleMode(hOut, &dwMode)) {
				this->log_error(to_string(GetLastError()));
			}
			dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
				if (!SetConsoleMode(hOut, dwMode)) {
					this->log_error(to_string(GetLastError()));
			}
			this->log_info("Windows");
		#else
			this->log_error("Use Windows!");
		#endif
	}
	this->log_info("Started");
}

//Constructor
engine::engine()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);
}

engine::~engine()
{
	this->log_info("End");
}

