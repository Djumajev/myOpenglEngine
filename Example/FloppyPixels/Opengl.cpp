#pragma once
//Intialization
void engine::Intialization()
{
	glClearColor(backgroung_RGB[0], backgroung_RGB[1], backgroung_RGB[2], 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-We / 2, We / 2, -He / 2, He / 2, 1.0, -5.0);
}

//Initilization
void engine::init(int argc, char** argv, int W, int H)
{
	We = W;
	He = H;
	OpenGLMain(argc, argv);
}

//timer
void engine::timer(int = 0)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(backgroung_RGB[0], backgroung_RGB[1], backgroung_RGB[2], 1.0);
	draw();
	Sleep(screen_delay);
	glFlush();

	glutTimerFunc(1, timer, 1);
}

//Main Opengl
void engine::OpenGLMain(int argc, char** argv)
{
	//OpenGL
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
	glutInitWindowSize(We, He);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Engine");
	Intialization();
	glutDisplayFunc(draw);
	timer();
	glutSpecialFunc(key_board);
	glutKeyboardFunc(process_normal_keys);
	glutPassiveMotionFunc(mouse_controller);
	glutMouseFunc(click);
	glutMainLoop();
}