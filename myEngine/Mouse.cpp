#pragma once
//Get mouse coordinates
int* engine::get_mouse()
{
	int* coordinates = new int[2];
	coordinates[0] = MouseX;
	coordinates[1] = MouseY;

	return coordinates;
}

//Is clicked
bool engine::is_clicked()
{
	return IsClicked;
}

//Get mouse click
int* engine::get_click(bool change_state)
{
	if (IsClicked)
	{
		int* coordinates = new int[2];
		coordinates[0] = MouseClickX;
		coordinates[1] = MouseClickY;
		if (change_state)
			IsClicked = false;

		return coordinates;
	}
	else return nullptr;
}

//Mouse Click
void engine::click(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		//Standart x=0 y=0 in left top corner
		MouseClickX = x - We / 2;
		MouseClickY = -y + He / 2;
		IsClicked = true;
	}
}

//Mouse
void engine::mouse_controller(int x, int y)
{
	//Standart x=0 y=0 in left top corner
	MouseX = x - We / 2;
	MouseY = -y + He / 2;
}
