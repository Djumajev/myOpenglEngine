#pragma once
//Clear screen
void engine::clear_screen()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

//Set delay
void engine::set_screen_delay(int delay)
{
	screen_delay = delay;
}

//Get delay
int engine::get_screen_delay()
{
	return screen_delay;
}

//Set fullscreen
void engine::set_fullscreen(bool a)
{
	fullscreen = a;
	if (fullscreen) glutFullScreen();
	else
	{
		glutReshapeWindow(We, He);
		glutPositionWindow(0, 0);
	}
}

//Get screen resolution
int* engine::get_resolution()
{
	int* res = new int[2];
	res[0] = We;
	res[1] = He;
	return res;
}

//Is fuulsreen
bool engine::is_fullscreen()
{
	return fullscreen;
}
