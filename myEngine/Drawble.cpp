#pragma once
//Line
void engine::line(int x, int y, int x2, int y2)
{
	glBegin(GL_LINES);
	glVertex2f(x, y);
	glVertex2f(x2, y2);
	glEnd();
}

//Set line width
void engine::set_linewidth(GLfloat a)
{
	glLineWidth(a);
}

//Set background
void engine::set_background(float red, float green, float blue)
{
	backgroung_RGB[0] = red;
	backgroung_RGB[1] = green;
	backgroung_RGB[2] = blue;
}

//Hexes
void engine::hexes(float x, float y, int Size, bool is_filled)
{
	//Mouse
	if ((MouseX > x - Size * 3 / 4 && MouseX < x + Size * 3 / 4) && (MouseY > y - Size * 3 / 4 && MouseY < y + Size * 3 / 4))
	{
		//glColor3f(HexThis.r+0.7+0.18, HexThis.g+0.7+0.18, HexThis.b+0.7+0.18);

		if ((MouseClickX > x - Size * 3 / 4 && MouseClickX < x + Size * 3 / 4) && (MouseClickY > y - Size * 3 / 4 && MouseClickY < y + Size * 3 / 4) && IsClicked == true)
		{
			//Somethings
			IsClicked = false;
		}
	}

	//Fill it
	if (is_filled == true)
	{
		glBegin(GL_TRIANGLES);
		for (int i = 1; i <= 6; i++)
		{
			glVertex2f(x, y);
			int angle = 60 * i + 30;
			glVertex2f(x + Size * cos(angle * PI / 180), y + Size * sin(angle * PI / 180));
			angle = 60 * (i + 1) + 30;
			glVertex2f(x + Size * cos(angle * PI / 180), y + Size * sin(angle * PI / 180));
		}
		glEnd();
	}

	//Lines
	glBegin(GL_LINE_LOOP);
	for (int i = 1; i <= 6; i++)
	{
		int angle = 60 * i + 30;
		glVertex2f(x + Size * cos(angle * PI / 180), y + Size * sin(angle * PI / 180));
	}
	glEnd();
}

//Quad
void engine::quad(float x, float y, int size_x, int size_y, bool is_filled)
{
	if (is_filled)
	{
		glBegin(GL_QUADS);
		glVertex2f(x - size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
	}
	else
	{
		glBegin(GL_LINES);
		glVertex2f(x - size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y - size_y / 2);
	}
	glEnd();

}

//Quadrilateral
void engine::quad(float x, float y, int size_x, bool is_filled)
{
	int size_y = size_x;
	if (is_filled)
	{
		glBegin(GL_QUADS);
		glVertex2f(x - size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
	}
	else
	{
		glBegin(GL_LINES);
		glVertex2f(x - size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y - size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x + size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y + size_y / 2);
		glVertex2f(x - size_x / 2, y - size_y / 2);
	}
	glEnd();

}

//Circles
void engine::circle(float x, float y, float r, int amountSegments, bool is_filled)
{
	if (!is_filled) glBegin(GL_LINE_LOOP);
	else glBegin(GL_LINES);
	for (int i = 0; i < amountSegments; i++)
	{
		GLdouble angle = 2.0 * PI * float(i) / float(amountSegments);
		GLfloat dx = r * cosf(angle);
		GLfloat dy = r * sinf(angle);
		glVertex2f(x + dx, y + dy);
		if (is_filled) glVertex2f(x, y);
	}
	glEnd();
}

//Text
void engine::write(float x, float y, string text)
{
	glRasterPos2f(x, y);
	for (unsigned int i = 0; i < text.length(); i++)
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, (int)text[i]); // GLUT_BITMAP_8_BY_13
}

//Text area
string engine::text_area(int x, int y, int size_x, int size_y, string input, float red, float green, float blue)
{
	int row = y + size_y / 2 - 20;

	this->quad(x, y, size_x, size_y, true);
	if ((MouseClickX > x - size_x / 2 && MouseClickX < x + size_x / 2) && (MouseClickY > y - size_y / 2 && MouseClickY < y + size_y / 2) && IsClicked)
	{
		//IsClicked = false;
		char letter = this->get_keyboard();
		if ((int)letter == 8 && input.size() > 0) input.resize(input.size() - 1);
		else if ((int)letter > 31 && (int)letter < 127) input += letter;
	}
	else {
		red = 0.5;
		green = 0.5;
		blue = 0.5;
	}

	this->set_color(red, green, blue);
	this->write(x - size_x / 2, row, input);
	return input;
}

//Set color
void engine::set_color(float red, float green, float blue)
{
	glColor3f(red, green, blue);
}

