#pragma once
//Timer on new thread
void engine::time_counter()
{
	while (started) {
		int new_time = time(0);
		Sleep(1);
		timer_int += time(0) - new_time;
	}
}

//Strat timer
void engine::start_timer()
{
	started = true;
	thread newth(&engine::time_counter, this);
	newth.detach();
}

//Stop timer
void engine::stop_timer()
{
	started = false;
	timer_int = 0;
}

//Return miliseconds
int engine::get_time()
{
	return timer_int;
}

//Return true or false
bool engine::is_timer()
{
	return started;
}
