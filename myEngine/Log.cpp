#pragma once

//String with colors
string formatRgbAnsi(string text, int r, int g, int b) {
	return "\033[38;2;" + to_string(r) + ";" +
		to_string(g) + ";" + to_string(b) +
		"m" + text + "\033[m";
}

//Log errors
void engine::log_error(string msg) {
	cout << formatRgbAnsi("[ERROR] ", 150, 0, 0) + msg << endl;
}

//Log info
void engine::log_info(string msg) {
	cout << formatRgbAnsi("[INFO] ", 150, 0, 150) + msg << endl;
}

//Log message    white
void engine::log_msg(string msg) {
	cout << msg << endl;
}

//Log message    colorfull
void engine::log_msg(string msg, int red, int green, int blue) {
	cout << formatRgbAnsi("[MSG] ", red, green, blue) + msg << endl;
}